import random


def play():
    """
    Guess the number.

    :return:
    """
    not_found = True
    number = random.randint(0, 100)
    challenge = "Adelante, elegí... Estoy segura de que perderás!\n --> "

    while not_found:

        n = int(input(challenge))

        if n > number:
            print("Más chico...")
        elif n < number:
            print("Más grande...")
        else:
            not_found = False
            print(f"...si serás puto, era {number}.")

    return None


def main():
    play()


if __name__ == "__main__":
    main()
